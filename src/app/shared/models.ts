export interface SearchResult {
    total: number;
    hits: SearchResultHit[];
    took: number;
}

export interface SearchResultHit {
    content: SearchResultContent;
    file_id: string;
    indexed_datetime: Date;
    meta: SearchResultMeta;
    tags: Array<SearchResultTag>;
    score: number;
}

export interface SearchResultContent {
    author: string;
    highlight: HighlightResult;
    language: string;
    length: number;
    ocr_performed: boolean;
    processed_datetime: Date;
    size: number;
    state: string;
    thumb_available: boolean;
    title: string;
    type: string;
}

export interface HighlightResult {
    text: Array<string>;
}

export interface SearchResultMeta {
    id: string;
    full_name: string;
    full_name_parts: string[];
    short_name: string;
    extension: string;
    extra: any[];
    source_id: string;
    created_datetime: string;
    updated_datetime: string;
    download_uri: string;
}

export interface SearchResultTag {
    id: string;
    type: string;
    name: string;
    indexed_datetime: string;
}
