import { Component, OnInit } from '@angular/core';
import { AmbarSearchService } from '../ambar-search.service';
import { FormControl } from '@angular/forms';
import { SearchResultHit } from '../shared/models';


@Component({
  selector: 'app-search-panel',
  templateUrl: './search-panel.component.html',
  styleUrls: ['./search-panel.component.css']
})
export class SearchPanelComponent implements OnInit {

  query = new FormControl('software');
  result: Array<SearchResultHit> = [];


  constructor(private ambarService: AmbarSearchService) { }

  async ngOnInit() {
    await this.onSearch(this.query.value).then(r => console.log(r));
  }

  async onSearch(query: string) {
    const result = await this.ambarService.search(query);
    this.result = result.hits;
    console.log(query);
    console.log(result);
  }

}
