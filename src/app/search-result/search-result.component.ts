import { Component, OnInit, Input } from '@angular/core';
import { SearchResultHit } from '../shared/models';
import { AmbarSearchService } from '../ambar-search.service';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {

  @Input() searchResultHit: SearchResultHit;

  constructor(private ambar: AmbarSearchService) { }

  ngOnInit() {
  }

  getDownloadUrl() {
    return this.ambar.getDownloadUrl(this.searchResultHit.meta.full_name);
  }

}
