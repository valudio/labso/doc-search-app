import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SearchResult } from './shared/models';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AmbarSearchService {
  private ambarUrl = environment.ambarUrl;

  constructor(private http: HttpClient) { }

  search(query: string): Promise<SearchResult> {
    return this.http.get<SearchResult>(`${this.ambarUrl}/search?query=${query}`).toPromise();
  }

  getDownloadUrl(download_path: string) {
    return `${this.ambarUrl}/files/download?path=${download_path}`;
  }

}
