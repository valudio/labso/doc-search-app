import { TestBed } from '@angular/core/testing';

import { AmbarSearchService } from './ambar-search.service';

describe('AmbarSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AmbarSearchService = TestBed.get(AmbarSearchService);
    expect(service).toBeTruthy();
  });
});
